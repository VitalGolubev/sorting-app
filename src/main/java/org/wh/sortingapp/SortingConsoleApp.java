package org.wh.sortingapp;

import java.util.Arrays;

public class SortingConsoleApp {

    public static void main(String[] args) {
        SortedArray sortedArray = new SortedArray(args,
            new Validator());

        System.out.println(sortedArray);
    }

}
