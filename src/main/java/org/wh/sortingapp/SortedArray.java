package org.wh.sortingapp;

import java.util.Arrays;

/**
 * Sorted Array class implementation
 *
 * @author Vitalii Holubiev
 */

public class SortedArray {
    Integer[] values;

    /**
     * Create new SortedArray instance
     *
     * @param args Array of strings that contain the values needed to be sorted and stored in Integer[] values
     * @param validator Validate that args are valid and contain only numbers and count of numbers greater than 2 and
     *                 less than 11
     *
     */
    public SortedArray(String[] args, Validator validator) {
        validator.validate(args);
        values = Arrays.stream(args).map(Integer::parseInt).sorted().toArray(Integer[]::new);

    }

    /**
     * Create new SortedArray instance
     *
     * @return values Sorted Array of Integer[]
     *
     */
    public Integer[] getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "SortedArray{" +
                "values=" + Arrays.toString(values) +
                '}';
    }
}
