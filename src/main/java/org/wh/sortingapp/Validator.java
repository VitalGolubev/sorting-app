package org.wh.sortingapp;

/**
 * Validator class implementation
 *
 * @author Vitalii Holubiev
 */
public class Validator {

    /**
     * @param input Array of strings that contain the values needed to be sorted and stored in Integer[] values
     * @return true if input is valid String[] array and contain only numbers and count of numbers
     * greater than 2 and less than 11
     *
     */
    public boolean validate(String[] input) {
        if (input == null || input.length > 10 || input.length <= 1) {
            return false;
        }

        for (String s : input) {
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

}
