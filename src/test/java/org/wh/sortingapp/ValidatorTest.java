package org.wh.sortingapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class ValidatorTest {

    Validator validator;
    String[] values;
    boolean expected;

    @Before
    public void setup() {
        validator = new Validator();
    }

    @Parameterized.Parameters(name = "Test Case: {index}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {null, false},
            {new String[] {"1"}, false},
            {new String[] {}, false},
            {new String[] {"1", "2"}, true},
            {new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, true},
            {new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}, false}
        });
    }

    public ValidatorTest(String[] values, boolean expected) {
        this.values = values;
        this.expected = expected;
    }

    @Test
    public void validateByParameters() {
        assertEquals(expected, validator.validate(values));
    }

}
