package org.wh.sortingapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortedArrayTest {


    Validator validator;
    String[] values;
    Integer[] expected;
    SortedArray sortedArray;

    @Before
    public void setup() {
        validator = new Validator();
    }

    @Parameterized.Parameters(name = "Test Case: {index}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {new String[] {"1", "3", "4", "7", "5", "8"}, new Integer[] {1, 3, 4, 5, 7, 8}},
            {new String[] {"9", "8", "7", "3", "2"}, new Integer[] {2, 3, 7, 8, 9}}
        });
    }

    public SortedArrayTest(String[] values, Integer[] expected) {
        this.values = values;
        this.expected = expected;
        sortedArray = new SortedArray(values, new Validator());

    }

    @Test
    public void testSorting() {
        assertArrayEquals(sortedArray.getValues(), expected);
    }

}
